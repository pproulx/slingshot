﻿using UnityEngine;
using System.Collections;

//This should be replace with a proper object pooling system in a real game project!
public class AutoDestroy : MonoBehaviour 
{
	[SerializeField] private float delay = 5;

	private void Start()
	{
		Invoke ("DoDestroy", delay);
	}

	private void DoDestroy()
	{
		Destroy (gameObject);
	}
}