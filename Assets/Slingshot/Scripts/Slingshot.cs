using UnityEngine;
using System.Collections;

public class Slingshot : MonoBehaviour
{
	private SlingshotController slingshotController;
	
	[System.NonSerialized] public Vector3 startPosition;
	[System.NonSerialized] public Vector3 endPosition;
	
	[SerializeField] private float maximumForce = 175;
	[System.NonSerialized] public Vector3 initialVelocity;
	
	[SerializeField] private float dragSensitivity = 1;
	[SerializeField] private GameObject projectilePrefab;
	[System.NonSerialized] public float normalizedLength = 1;
	
	[System.NonSerialized] public Vector3 direction;
	public Vector3 gravityModifier = new Vector3(0,-30,0);

	[SerializeField] private bool preventDownShooting = true;

	private void Awake()
	{
		slingshotController = GetComponent<SlingshotController>();
	}

	private void OnPull()
	{
		startPosition = Input.mousePosition;
	}

	private void OnRelease()
	{
		Shoot();
	}
	
	private void OnAiming()
	{
		endPosition = Input.mousePosition;

		if (preventDownShooting)
		{
			endPosition.y = Mathf.Min(endPosition.y, startPosition.y);
		}
		
		direction = (startPosition - endPosition).normalized;
		initialVelocity =  direction * Mathf.Min((startPosition - endPosition).magnitude * dragSensitivity, maximumForce);
		
		normalizedLength = initialVelocity.magnitude / maximumForce;
	}

	private void Shoot()
	{
		Projectile newProjectile = (Instantiate(projectilePrefab) as  GameObject).GetComponent<Projectile>();
		newProjectile.transform.parent = transform;
		newProjectile.transform.position = transform.position;
		newProjectile.GetComponent<Rigidbody>().velocity = initialVelocity;
		newProjectile.slingshot = this;
	}
	
	private void OnEnable()
	{
		slingshotController.onPull    += OnPull;
		slingshotController.onRelease += OnRelease;
		slingshotController.onAiming  += OnAiming;
	}
	
	private void OnDisable()
	{
		slingshotController.onPull    -= OnPull;
		slingshotController.onRelease -= OnRelease;
		slingshotController.onAiming  -= OnAiming;
	}
}
