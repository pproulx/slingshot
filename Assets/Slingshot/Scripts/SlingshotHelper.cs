using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Slingshot))]
[RequireComponent (typeof (SlingshotController))]
public class SlingshotHelper : MonoBehaviour 
{
	[SerializeField] private Transform pointPrefab;
	[SerializeField] private int pointCount = 5;
	[SerializeField] private float pointScaleProgression = 0.005f;
	[SerializeField] private float pointAlphaProgression = 0.08f;
	[SerializeField] private float pointFrequency = 0.1f;
	[SerializeField] private float pointStartOffset = 0.1f;
	[SerializeField] private Color baseColor = new Color(1, 1, 1);

	private Slingshot slingshot;
	private SlingshotController slingshotController;
	private Transform[] points;

	private void Awake()
	{
		slingshot = GetComponent<Slingshot>();
		slingshotController = GetComponent<SlingshotController>();
	}
	
	private void Start() 
	{
		InitializePoints();
	}

	private void InitializePoints()
	{
		points = new Transform[pointCount];
		
		for (int i = 0; i < pointCount; i++)
		{
			points[i] = Instantiate(pointPrefab) as Transform;
			points[i].parent = this.transform;
			points[i].localScale = Vector3.one * (points[i].localScale.x - (i * pointScaleProgression));
			points[i].GetComponent<Renderer>().material.color = new Color(baseColor.r, 
			                                              baseColor.g, 
			                                              baseColor.b, 
			                                              points[i].GetComponent<Renderer>().material.color.a - (i * pointAlphaProgression));
			points[i].name = "Point " + i;
			points[i].gameObject.SetActive(false);
		}
	}

	private void OnRelease()
	{
		for (int i = 0; i < pointCount; i++)
		{
			points[i].gameObject.SetActive(false);
		}
	}
	
	private void OnAiming()
	{
		if (!points[0].gameObject.activeSelf)
		{
			for (int i = 0; i < pointCount; i++)
			{
				points[i].gameObject.SetActive(true);
			}
		}
		
		for (int i = 0; i < pointCount; i++)
		{
			points[i].position = GetPointPosition((i+pointStartOffset) * pointFrequency);
		}
	}

	private Vector3 GetPointPosition(float t)
	{
		Vector3 initialPosition = this.transform.position;
		Vector3 velocity = slingshot.initialVelocity * t;
		Vector3 acceleration = new Vector3(0, 0.5f * (Physics.gravity.y + slingshot.gravityModifier.y) * t * t, 0);
		
		return initialPosition + velocity + acceleration;
	}

	private void OnEnable()
	{
		slingshotController.onRelease += OnRelease;
		slingshotController.onAiming  += OnAiming;
	}
	
	private void OnDisable()
	{
		slingshotController.onRelease -= OnRelease;
		slingshotController.onAiming  -= OnAiming;
	}
}
