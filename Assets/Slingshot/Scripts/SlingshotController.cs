using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Slingshot))]
public class SlingshotController : MonoBehaviour 
{
	private const float MINIMUM_DRAG_DISTANCE_SQUARED = 10;
	private Slingshot slingshot;
	
	public delegate void Pull();
	public event Pull onPull;

	public delegate void Release();
	public event Release onRelease;
	
	public delegate void Aiming();
	public event Aiming onAiming;
	
	private bool isAiming;
	
	private void Awake()
	{
		slingshot = GetComponent<Slingshot>();
	}
		
	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			if (onPull != null)
			{
				onPull();
			}
		}
		
		if (Input.GetMouseButtonUp(0) && isAiming)
		{
			if (onRelease != null)
			{
				isAiming = false;
				onRelease();
			}
		}
		
		if (Input.GetMouseButton(0))
		{
			if (!isAiming && (slingshot.startPosition - Input.mousePosition).sqrMagnitude > (MINIMUM_DRAG_DISTANCE_SQUARED * MINIMUM_DRAG_DISTANCE_SQUARED))
			{
				isAiming = true;
			}
			
			if (isAiming)
			{
				if (onAiming != null)
				{
					onAiming();
				}
			}
		}
	}
}
