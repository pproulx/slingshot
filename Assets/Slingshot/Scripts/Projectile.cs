#define TRACE_POSITION

using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
	[System.NonSerialized] public Slingshot slingshot;
	private new Rigidbody rigidbody;

#if TRACE_POSITION
	private /*const*/ Color DEBUG_COLOR = Color.red;
	private /*const*/ float DEBUG_DURATION = 1f;
	private Vector3 previousPosition;

	private void Start()
	{
		previousPosition = transform.position;
	}
#endif	

	private void Awake()
	{
		rigidbody = GetComponent<Rigidbody>();
	}

#if TRACE_POSITION
	private void Update()
	{
		Debug.DrawLine(previousPosition, transform.position, DEBUG_COLOR, DEBUG_DURATION);
		previousPosition = transform.position;
	}
#endif

	private void FixedUpdate()
	{
		rigidbody.AddForce(slingshot.gravityModifier);	
	}
}